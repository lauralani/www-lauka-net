---
layout: layouts/home.njk
title: "Entwurf eines Gesetzes über die Selbstbestimmung"
eleventyExcludeFromCollections: true
---

__Referentenentwurf des Bundesministeriums für Familie, Senioren, Frauen und Jugend und des Bundesministeriums der Justiz – Entwurf eines Gesetzes über die Selbstbestimmung in Bezug auf den Geschlechtseintrag und zur Änderung weiterer Vorschriften – Stand: 26. April 2023__

Der Bundestag hat das folgende Gesetz beschlossen:

**Artikel 1 - Gesetz über die Selbstbestimmung in Bezug auf den Geschlechtseintrag (SBGG)**

**§ 1 Ziel des Gesetzes**

Ziel dieses Gesetzes ist es,

1\. die personenstandsrechtliche Geschlechtszuordnung und die Vornamenswahl von der Einschätzung dritter Personen zu lösen und die Selbstbestimmung der betroffenen Person zu stärken,

2\. das Recht jeder Person auf Achtung und respektvolle Behandlung in Bezug auf die Geschlechtsidentität zu verwirklichen.

(2) Medizinische Maßnahmen werden in diesem Gesetz nicht geregelt.

**§ 2 Erklärungen zum Geschlechtseintrag und zu den Vornamen**

(1) Jede Person, deren Geschlechtsidentität von ihrem Geschlechtseintrag im Personenstandsregister abweicht, kann gegenüber dem Standesamt nach Maßgabe des § 45b des Personenstandsgesetzes erklären, dass die Angabe zu ihrem Geschlecht in einem deutschen Personenstandseintrag geändert werden soll, indem sie durch eine andere der in § 22 Absatz 3 des Personenstandsgesetzes vorgesehenen Angaben ersetzt oder gestrichen wird. Liegt kein deutscher Personenstandseintrag vor, so kann die Person gegenüber dem Standesamt nach Maßgabe des § 45b des Personenstandsgesetzes erklären, welche der in § 22 Absatz 3 des Personenstandsgesetzes vorgesehenen Angaben für sie maßgeblich ist oder dass auf die Angabe einer Geschlechtsbezeichnung verzichtet wird.

(2) Die Person hat mit ihrer Erklärung zu versichern, dass

1\. der gewählte Geschlechtseintrag beziehungsweise die Streichung des Geschlechtseintrags ihrer Geschlechtsidentität am besten entspricht,

2\. ihr die Tragweite der durch die Erklärung bewirkten Folgen bewusst ist.

(3) Mit der Erklärung können auch ein neuer Vorname oder mehrere neue Vornamen bestimmt werden.

(4) Jede Person, deren Vorname oder Vornamen nicht ihrer Geschlechtsidentität entsprechen, kann gegenüber dem Standesamt auch nur ihren Vornamen oder ihre Vornamen neu bestimmen. Dabei hat sie zu versichern, dass

1\. die gewählten Vornamen ihrer Geschlechtsidentität am besten entsprechen,

2\. ihr die Tragweite der durch die Erklärung bewirkten Folgen bewusst ist.

**§ 3 Erklärungen von Minderjährigen und Personen mit Betreuer**

(1) Eine beschränkt geschäftsfähige minderjährige Person, die das 14. Lebensjahr vollendet hat, kann die Erklärungen zur Änderung des Geschlechtseintrags und der Vornamen (§ 2) nur selbst abgeben, bedarf hierzu jedoch der Zustimmung ihres gesetzlichen Vertreters. Stimmt der gesetzliche Vertreter nicht zu, so ersetzt das Familiengericht die Zustimmung, wenn die Änderung des Geschlechtseintrags oder der Vornamen dem Kindeswohl nicht widerspricht.

(2) Ist die minderjährige Person geschäftsunfähig oder hat sie das 14. Lebensjahr noch nicht vollendet, kann nur der gesetzliche Vertreter die Erklärungen zur Änderung des Geschlechtseintrags und der Vornamen (§ 2) für die Person abgeben. Ein Vormund bedarf hierzu der Genehmigung des Familiengerichts; das Familiengericht erteilt die Genehmigung, wenn die Erklärung unter Berücksichtigung der Rechte des Mündels aus § 1788 des Bürgerlichen Gesetzbuchs dem Wohl des Mündels nicht widerspricht.

(3) Für eine volljährige Person, für die in dieser Angelegenheit ein Betreuer bestellt und ein Einwilligungsvorbehalt nach § 1825 des Bürgerlichen Gesetzbuchs angeordnet ist, kann nur der Betreuer die Erklärungen zur Änderung des Geschlechtseintrags und der Vornamen nach § 2 abgeben; er bedarf hierzu der Genehmigung des Betreuungsgerichts. Entsprechendes gilt, wenn ein geschäftsunfähiger Volljähriger, für den in dieser Angelegenheit ein Betreuer bestellt ist, die Erklärung nicht selbst abgeben kann. Das Betreuungsgericht erteilt die Genehmigung, wenn die Erklärung einem nach § 1821 Absatz 2 bis 4 des Bürgerlichen Gesetzbuchs zu beachtenden Wunsch oder dem mutmaßlichen Willen des Betreuten entspricht.

**§ 4 Wirksamkeit; Rücknahme der Erklärung**

Die Änderung des Geschlechtseintrags oder der Vornamen wird erst drei Monate nach der Erklärung gemäß § 2 im Personenstandsregister eingetragen und wirksam. Innerhalb dieser Frist kann die Person ihre Erklärung schriftlich gegenüber dem Standesamt, bei dem die Erklärung nach § 2 abgegeben wurde, zurücknehmen.

**§ 5 Sperrfrist; Vornamenbestimmung bei Rückänderung**

(1) Vor Ablauf eines Jahres nach der Eintragung der Änderung des Geschlechtseintrags und der Vornamen kann die Person keine erneute Erklärung nach § 2 abgeben. Dies gilt nicht in den Fällen des § 3.

(2) Will eine Person mit einer erneuten Erklärung eine Rückänderung zu einem früheren Geschlechtseintrag und eine Vornamensänderung bewirken, so kann sie nur den oder die Vornamen zu ihrem oder ihren neuen Vornamen bestimmen, den oder die sie vor der Änderung dieses früheren Geschlechtseintrags zuletzt geführt hat. Dasselbe gilt in den Fällen des § 2 Absatz 4, wenn die erneute Erklärung einer Vornamensänderung mit einer Geschlechtsidentität begründet wird, die einem früheren oder unverändert gebliebenen Geschlechtseintrag entspricht. Abweichend von den Sätzen 1 und 2 kann die betroffene Person einen anderen Vornamen beziehungsweise andere Vornamen bestimmen, wenn dies aus schwerwiegenden Gründen zu ihrem Wohl erforderlich ist.

**§ 6 Wirkungen der Änderung des Geschlechtseintrags und der Vornamen**

(1) Der jeweils aktuelle Geschlechtseintrag und die jeweils aktuellen Vornamen sind im Rechtsverkehr maßgeblich, soweit auf die personenstandsrechtliche Geschlechtszuordnung oder die Vornamen Bezug genommen wird und durch Gesetz nichts anderes bestimmt ist.

(2) Betreffend den Zugang zu Einrichtungen und Räumen sowie die Teilnahme an Veranstaltungen bleiben das Hausrecht des jeweiligen Eigentümers oder Besitzers und das Recht juristischer Personen, ihre Angelegenheiten durch Satzung zu regeln, unberührt.

(3) Die Bewertung sportlicher Leistungen kann unabhängig von dem aktuellen Geschlechtseintrag geregelt werden.

(4) Auf den aktuellen Geschlechtseintrag kommt es nicht an, wenn medizinische Maßnahmen zu ergreifen sind.

**§ 7 Quotenregelungen**

(1) Wenn für die Besetzung von Gremien oder Organen durch Gesetz eine Mindestanzahl oder ein Mindestanteil an Mitgliedern weiblichen und männlichen Geschlechts vorgesehen ist, so ist das im Personenstandsregister eingetragene Geschlecht der Mitglieder zum Zeitpunkt der Besetzung maßgeblich.

(2) Eine nach der Besetzung erfolgte Änderung des Geschlechtseintrags eines Mitglieds im Personenstandsregister ist bei der nächsten Besetzung eines Mitglieds zu berücksichtigen. Reicht dabei die Anzahl der neu zu besetzenden Sitze nicht aus, um die gesetzlich vorgesehene Mindestanzahl oder den gesetzlich vorgesehenen Mindestanteil an Mitgliedern zu erreichen, so sind diese Sitze nur mit Personen des unterrepräsentierten Geschlechts zu besetzen, um dessen Anteil sukzessive zu steigern.

(3) Die Absätze 1 und 2 sind nur anzuwenden, wenn nichts anderes geregelt ist.

**§ 8 Anwendbarkeit von Rechtsvorschriften zu Gebär- und Zeugungsfähigkeit**

(1) Gesetze und Verordnungen, die Regelungen über Schwangerschaft, Gebärfähigkeit, Übertragung von und Eizellen oder Embryonen oder künstliche Befruchtung treffen, gelten unabhängig von dem im Personenstandsregister für sie eingetragenen Geschlecht für 1. schwangere oder gebärfähige Personen, 2. Personen, die schwanger oder gebärfähig werden wollen, 3. Personen, die ein Kind geboren haben oder stillen und 4. Personen, denen Eizellen oder Embryonen entnommen oder übertragen werden oder bei denen eine künstliche Befruchtung durchgeführt wird.

(2) Gesetze und Verordnungen, die an die Entnahme oder Übertragung von Samenzellen, an die Stellung als leiblicher Vater oder daran anknüpfen, dass ein Mann einer Mutter während der Empfängniszeit beigewohnt hat, gelten unabhängig von dem im Personenstandsregister für sie eingetragenen Geschlecht für Personen,

1\. die zeugungsfähig waren oder sind,

2\. die ein Kind gezeugt haben oder hätten zeugen können und

3\. die Samenzellen gespendet haben oder denen Samenzellen entnommen werden.

**§ 9 Zuordnung zum männlichen Geschlecht im Spannungs- und Verteidigungsfall**

Die rechtliche Zuordnung einer Person zum männlichen Geschlecht bleibt, soweit es den Dienst an der Waffe auf Grundlage des Artikels 12a des Grundgesetzes und hierauf beruhender Gesetze betrifft, für die Dauer des Spannungs- oder Verteidigungsfalls nach Artikel 80a des Grundgesetzes bestehen, wenn in unmittelbarem zeitlichen Zusammenhang mit diesem die Änderung des Geschlechtseintrags von „männlich“ zu „weiblich“ oder „divers“ oder die Streichung der Angabe zum Geschlechtseintrag erklärt wird, sofern dies im Einzelfall keine unbillige Härte darstellen würde. Der zeitliche Zusammenhang ist unmittelbar ab einem Zeitpunkt von zwei Monaten vor Feststellung des Spannungs- oder Verteidigungsfalls sowie während desselben gegeben.

**§ 10 Änderung von Registern und Dokumenten**

(1) Sind der Geschlechtseintrag und die Vornamen einer Person im Personenstandsregister geändert worden, so kann sie verlangen, dass auch Einträge zu ihrem Geschlecht und ihren Vornamen in anderen amtlichen Registern geändert werden, wenn dem keine besonderen Gründe des öffentlichen Interesses entgegenstehen.

(2) Die Person kann auch verlangen, dass amtliche und nichtamtliche Dokumente, soweit diese Angaben zum Geschlecht und zu den Vornamen enthalten, mit dem geänderten Geschlechtseintrag und den geänderten Vornamen neu ausgestellt werden. Dies gilt insbesondere für folgende Dokumente:

1\. Zeugnisse und andere Leistungsnachweise,

2\. Ausbildungs- und Dienstverträge,

3\. Besitzstandsurkunden,

4\. Führerscheine,

5\. Sozialversicherungs- und Krankenversicherungsausweise,

6\. Zahlungskarten und

7\. Sterbeurkunden über den Tod von Ehegatten.

(3) Der Anspruch nach Absatz 2 richtet sich gegen die öffentliche oder private Stelle oder Person,

1\. die das zu ändernde Dokument ausgestellt hat,

2\. die Vertragspartner der nach Absatz 2 berechtigten Person ist oder

3\. die sonst zur Ausstellung einer Zweitschrift befugt ist.

Die nach Absatz 2 berechtigte Person hat die angemessenen Kosten der Neuausstellung zu tragen.

**§ 11 Eltern-Kind-Verhältnis**

(1) Der Geschlechtseintrag im Personenstandsregister ist für das nach den §§ 1591 und 1592 Nummer 3 des Bürgerlichen Gesetzbuchs bestehende oder künftig begründete Rechtsverhältnis zwischen einer Person und ihren Kindern unerheblich. Für das nach § 1592 Nummer 1 oder 2 des Bürgerlichen Gesetzbuchs bestehende oder künftig begründete Rechtsverhältnis zwischen einer Person und ihren Kindern ist ihr Geschlechtseintrag im Personenstandsregister zum Zeitpunkt der Geburt des Kindes maßgeblich.

(2) Das bestehende Rechtsverhältnis zwischen einer Person und ihren angenommenen Kindern bleibt durch eine Änderung des Geschlechtseintrags unberührt. Für das künftig begründete Rechtsverhältnis zwischen einer Person und ihren angenommenen Kindern ist ihr Geschlechtseintrag im Personenstandsregister zum Zeitpunkt der Annahme maßgeblich.

**§ 12 Geschlechtsneutrale Regelungen**

Gesetzliche Regelungen, die sich auf Männer und Frauen beziehen und für beide Geschlechter dieselben Rechtsfolgen vorsehen, gelten für Personen unabhängig von der im Personenstandsregister eingetragenen Geschlechtsangabe und auch dann, wenn keine Angabe eingetragen ist.

**§ 13 Offenbarungsverbot**

(1) Ist der Geschlechtseintrag einer Person aufgrund des § 2 Absatz 1 oder sind die Vornamen einer Person aufgrund des § 2 Absatz 3 oder 4 geändert worden, so dürfen die bis zur Änderung eingetragene Geschlechtszugehörigkeit und die bis zur Änderung eingetragenen Vornamen ohne Zustimmung dieser Person nicht offenbart oder ausgeforscht werden, es sei denn, dass besondere Gründe des öffentlichen Interesses dies erfordern oder ein rechtliches Interesse glaubhaft gemacht wird.

(2) Der frühere und der derzeitige Ehegatte, die Verwandten in gerader Linie und der andere Elternteil eines Kindes der in Absatz 1 genannten Person sind nur dann verpflichtet, den geänderten Geschlechtseintrag und die geänderten Vornamen anzugeben, wenn dies für die Führung öffentlicher Bücher und Register oder im Rechtsverkehr erforderlich ist. Satz 1 gilt nicht für

1\. Ehegatten aus nach der Änderung des Geschlechtseintrags geschlossenen Ehen,

2\. nach der Änderung des Geschlechtseintrags geborene oder angenommene Kinder,

3\. den anderen Elternteil von nach der Änderung des Geschlechtseintrags geborenen oder angenommenen Kindern.

**§ 14 Bußgeldvorschriften**

(1) Ordnungswidrig handelt, wer entgegen § 13 Absatz 1 die Geschlechtszugehörigkeit oder einen Vornamen offenbart und dadurch die betroffene Person absichtlich schädigt.

(2) Die Ordnungswidrigkeit kann mit einer Geldbuße bis zu zehntausend Euro geahndet werden.

**§ 15 Übergangsvorschriften**

(1) Am … \[einsetzen: Datum des Inkrafttretens dieses Gesetzes nach Artikel 14 Satz 1\] anhängige Verfahren nach dem Transsexuellengesetz in der bis einschließlich … \[einsetzen: Datum des Tages vor dem Inkrafttreten dieses Gesetzes nach Artikel 14 Satz 1\] geltenden Fassung werden nach dem bis einschließlich… \[einsetzen: Datum des Tages vor dem Inkrafttreten dieses Gesetzes nach Artikel 14 Satz 1\] geltenden Recht weitergeführt.

(2) Die §§ 5 bis 12 gelten entsprechend für Änderungen des Geschlechtseintrags und der Vornamen, die vorgenommen wurden auf Grund der jeweils bis einschließlich … \[einsetzen: Datum des Tages vor dem Inkrafttreten dieses Gesetzes nach Artikel 14 Satz 1\] geltenden Fassung

1\. des Transsexuellengesetzes und

2\. des § 45b des Personenstandsgesetzes.

**Artikel 2 - Änderung des Passgesetzes**

Das Passgesetz vom 19. April 1986 (BGBl. I S. 537), das zuletzt durch Artikel 1 des Gesetzes vom 5. Juli 2021 (BGBl. I S. 2281) geändert worden ist, wird wie folgt geändert:

1\. § 4 Absatz 1 Satz 5 und 6 wird wie folgt gefasst: „Abweichend von den Sätzen 3 und 4 ist einem Passbewerber, der nach § 2 Absatz 4 des Gesetzes über die Selbstbestimmung in Bezug auf den Geschlechtseintrag nur seine Vornamen geändert hat, auf Antrag ein Pass mit der Angabe des Geschlechts auszustellen, das den neuen Vornamen entspricht. Passbewerbern, deren Geschlecht im Personenstandseintrag weder mit „männlich“ noch mit „weiblich“ angegeben ist, kann auf Antrag abweichend von den Sätzen 3 und 4 auch ein Pass mit der Angabe „männlich“ beziehungsweise „weiblich“ ausgestellt werden.“

2\. § 6 Absatz 2a wird wie folgt gefasst: „(2a)Beantragt ein Passbewerber gemäß § 4 Absatz 1 Satz 5 nach einer Vornamensänderung die Eintragung einer entsprechenden Geschlechtsangabe oder die Bezeichnung mit „X“, hat er einen Nachweis über die Änderung der Vornamen nach § 2 Absatz 4 des Gesetzes über die Selbstbestimmung in Bezug auf den Geschlechtseintrag vorzulegen. Eintragungen des Geschlechts im Pass, die von Eintragungen im Personenstandsregister abweichen, kommt keine weitere Rechtswirkung zu.“

3\. Dem § 28 wird folgender Absatz 4 angefügt: „(4) § 4 Absatz 1 Satz 5 und 6 und § 6 Absatz 2a gelten entsprechend für Änderungen des Geschlechtseintrags und der Vornamen, die vorgenommen wurden auf Grund der jeweils bis einschließlich … \[einsetzen: Datum des Tages vor dem Inkrafttreten dieses Gesetzes nach Artikel 14 Satz 1\] geltenden Fassung

1\. des Transsexuellengesetzes und

2\. des § 45b des Personenstandsgesetzes.“

**Artikel 3 - Änderung des Personenstandsgesetzes**

Das Personenstandsgesetz vom 19. Februar 2007 (BGBl. I S. 122), das zuletzt durch Artikel 1 des Gesetzes vom 19. Oktober 2022 (BGBl. I S. 1744) geändert worden ist, wird wie folgt geändert:

1\. Die Inhaltsübersicht wird wie folgt geändert:

a) Die Angabe zu § 45b wird wie folgt gefasst: „§ 45b Erklärungen zur Änderung des Geschlechtseintrags und der Vornamen“.

b) Die Angabe zu § 78 wird wie folgt gefasst: „§ 78 Übergangsregelung“.

2\. In § 16 Absatz 2 Satz 3 werden die Wörter „auf Grund des Transsexuellengesetzes“ durch die Wörter „nach § 2 Absatz 3 oder 4 des Gesetzes über die Selbstbestimmung in Bezug auf den Geschlechtseintrag“ ersetzt.

3\. § 27 Absatz 3 wird wie folgt geändert:

a) In Nummer 3 werden nach dem Wort „Feststellung“ die Wörter „oder die Änderung“ eingefügt.

b) In Nummer 4 werden die Wörter „oder die Änderung des Geschlechts“ durch die Wörter „des einzutragenden Geschlechts oder die Änderung des Geschlechtseintrags“ ersetzt.

c) Nach Nummer 4 wird folgende Nummer 5 eingefügt: „5. die Änderung des Geschlechtseintrags oder der Vornamen eines Elternteils,“.

d) Die bisherigen Nummer 5 wird Nummer 6.

4\. § 45b wird wie folgt gefasst: „§ 45b Erklärungen zur Änderung des Geschlechtseintrags und der Vornamen

(1) Die Erklärungen zur Änderung des Geschlechtseintrags und der Vornamen nach § 2 des Gesetzes über die Selbstbestimmung in Bezug auf den Geschlechtseintrag müssen öffentlich beglaubigt werden; sie können auch von den Standesbeamten beglaubigt oder beurkundet werden. Ist die Zustimmung eines gesetzlichen Vertreters erforderlich, gilt dasselbe für dessen Erklärung.

(2) Für die Entgegennahme der Erklärungen ist das Standesamt zuständig, das das Geburtenregister für die Person, deren Geschlechtseintrag oder Vornamen geändert werden soll, führt. Ist die Geburt nicht in einem deutschen Geburtenregister beurkundet, so ist das Standesamt zuständig, das das Eheregister oder Lebenspartnerschaftsregister der Person führt. Ergibt sich danach keine Zuständigkeit, so ist das Standesamt zuständig, in dessen Zuständigkeitsbereich die Person ihren Wohnsitz hat oder zuletzt hatte oder ihren gewöhnlichen Aufenthalt hat. Ergibt sich auch danach keine Zuständigkeit, so ist das Standesamt I in Berlin zuständig. Das Standesamt I in Berlin führt ein Verzeichnis der nach den Sätzen 3 und 4 entgegengenommenen Erklärungen.“

5\. Dem § 57 wird folgender Absatz 3 angefügt: „(3) Auf Verlangen von Personen, deren Vornamen nach § 2 Absatz 3 oder 4 des Gesetzes über die Selbstbestimmung in Bezug auf den Geschlechtseintrag geändert worden sind, werden in die Eheurkunde die vor der Eheschließung geführten Vornamen nicht aufgenommen.“

6\. Dem § 58 wird folgender Absatz 3 angefügt: „(3) Auf Verlangen von Personen, deren Vornamen nach § 2 Absatz 3 oder 4 des Gesetzes über die Selbstbestimmung in Bezug auf den Geschlechtseintrag geändert worden sind, werden in die Lebenspartnerschaftsurkunde die vor der Begründung der Lebenspartnerschaft geführten Vornamen nicht aufgenommen.“

7\. § 63 Absatz 2 wird wie folgt gefasst: „(2) Ist der Geschlechtseintrag einer Person nach § 2 Absatz 1 des Gesetzes über die Selbstbestimmung in Bezug auf den Geschlechtseintrag oder sind die Vornamen einer Person nach § 2 Absatz 3 oder 4 des Gesetzes über die Selbstbestimmung in Bezug auf den Geschlechtseintrag geändert worden, so gilt abweichend von § 62:

1\. eine Personenstandsurkunde aus dem Geburtseintrag darf nur der betroffenen Person selbst erteilt werden,

2\. eine Ehe- oder Lebenspartnerschaftsurkunde aus dem Ehe- oder Lebenspartnerschaftseintrag darf nur der betroffenen Person selbst sowie ihrem Ehegatten oder Lebenspartner erteilt werden. Diese Beschränkungen entfallen mit dem Tod der betroffenen Person; § 13 des Gesetzes über die Selbstbestimmung in Bezug auf den Geschlechtseintrag bleibt unberührt.“

8\. § 78 wird wie folgt gefasst:

„§ 78 Übergangsregelung - Die Vorschriften für Änderungen des Geschlechtseintrags und der Vornamen nach dem Gesetz über die Selbstbestimmung in Bezug auf den Geschlechtseintrag gelten auch für die Änderungen, die vorgenommen wurden auf Grund der jeweils bis ein schließlich … \[einsetzen: Datum des Tages vor dem Inkrafttreten dieses Gesetzes nach Artikel 14 Satz 1\] geltenden Fassung 1. des Transsexuellengesetzes und 2. des § 45b des Personenstandsgesetzes.“

**Artikel 4 - Änderung der Personenstandsverordnung**

Die Personenstandsverordnung vom 22. November 2008 (BGBl. I S. 2263), die zuletzt durch Artikel 2 des Gesetzes vom 19. Oktober 2022 (BGBl. I S. 1744) geändert worden ist, wird wie folgt geändert:

1\. § 42 wird wie folgt geändert: a) Absatz 2 wird wie folgt geändert: aa) In Satz 3 werden die Wörter „weder dem männlichen noch“ durch das Wort „nicht“ ersetzt. bb) In Satz 4 werden die Wörter „weder dem männlichen noch dem weiblichen“ durch die Wörter „zum Zeitpunkt der Geburt nicht dem männlichen“ und wird die Angabe „BGB“ durch die Wörter „des Bürgerlichen Gesetzbuchs“ ersetzt.

b) Nach Absatz 2 wird folgender Absatz 2a eingefügt: „(2a)Auf Verlangen der als „Mutter“ oder „Vater“ in einer Geburtsurkunde eingetragenen Person wird diese Bezeichnung durch „Elternteil“ ersetzt, wenn sie

1\. ihren Geschlechtseintrag geändert hat oder

2\. ohne Änderung des Geschlechtseintrags weder dem männlichen noch dem weiblichen Geschlecht zugeordnet ist.

Ist ein Elternteil des Kindes mit der Bezeichnung „Elternteil“ eingetragen, so wird auf Verlangen des anderen Elternteils dessen Eintrag als „Mutter“ oder „Vater“ ebenfalls durch die Bezeichnung „Elternteil“ ersetzt.“

2\. In § 46 Nummer 3 werden nach dem Wort „oder nach § 45b des Gesetzes“ durch ein Komma und die Wörter „nach § 45b des Gesetzes oder nach § 2 des Gesetzes über die Selbstbestimmung in Bezug auf den Geschlechtseintrag“ ersetzt.

3\. In § 56 Absatz 1 Nummer 1 Buchstabe d in dem Satzteil vor Doppelbuchstabe aa werden nach dem Wort „Transsexuellengesetzes“ die Wörter „in der bis einschließlich … \[einsetzen: Datum des Tages vor dem Inkrafttreten dieses Gesetzes nach Artikel 14 Satz 1\] geltenden Fassung“ eingefügt.

4\. \[Folgende Anlage 13 wird angefügt:\]

**Artikel 5 - Änderung des Aufenthaltsgesetzes**

Das Aufenthaltsgesetz in der Fassung der Bekanntmachung vom 25. Februar 2008 (BGBl. I S. 162), das zuletzt durch Artikel 5 des Gesetzes vom 21. Dezember 2022 (BGBl. I S. 2847) geändert worden ist, wird wie folgt geändert:

1\. § 78 Absatz 1 Satz 6 wird wie folgt gefasst: „Auf Antrag können Dokumente nach den Sätzen 1 und 2 bei einem Antragsteller, dessen Geschlechtseintrag weder mit „männlich“ noch mit „weiblich“ angegeben ist, mit der Angabe „männlich“ beziehungsweise „weiblich“ ausgestellt werden.“

2\. § 78a Absatz 2 Satz 2 wird wie folgt gefasst: „Auf Antrag kann in der Zone für das automatische Lesen bei einem Inhaber, dessen Geschlechtseintrag weder mit „männlich“ noch mit „weiblich“ angegeben ist, die Angabe des Geschlechts mit der Angabe „männlich“ beziehungsweise „weiblich“ aufgenommen werden.“

**Artikel 6 - Änderung der Aufenthaltsverordnung**

§ 4 Absatz 2 Satz 2 der Aufenthaltsverordnung vom 25. November 2004 (BGBl. I S. 2945), die zuletzt durch Artikel 4 der Verordnung vom 20. August 2021 (BGBl. I S. 3682) geändert worden ist, wird wie folgt gefasst:

„Auf Antrag kann der Passersatz nach Absatz 1 Satz 1 Nummer 1, 3 und 4 bei einem Inhaber, dessen Geschlechtseintrag weder mit „männlich“ noch mit „weiblich“ angegeben ist, mit der Angabe „männlich“ beziehungsweise „weiblich“ ausgestellt werden“.
