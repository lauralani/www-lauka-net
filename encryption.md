---
layout: layouts/home.njk
title: "lauka.net - Encryption Keys"
eleventyExcludeFromCollections: true
date: 2023-06-23T10:02:23+02:00
---
[⇠ back home](/)


# Lauras Encryption Keys
Last modified: `{{ page.date | timestamp }}`
## GPG Keys
### mail@lauka.net

Links: 
- [www.lauka.net](https://www.lauka.net/certificates/gpg/mail@lauka.net.asc)
- [keys.openpgp.org](https://keys.openpgp.org/vks/v1/by-fingerprint/AFB583D770414388D3C7D84E0BEBC883A64A806E)


Fingerprint: `AFB5 83D7 7041 4388 D3C7 D84E 0BEB C883 A64A 806E`


### laura@laurakalb.de

Links: 
- [www.lauka.net](https://www.lauka.net/certificates/gpg/laura@laurakalb.de.asc)
- [keys.openpgp.org](https://keys.openpgp.org/vks/v1/by-fingerprint/ABABDCBE322AD93D6F2577A45EEB5ED5B41E3C64)


Fingerprint: `ABAB DCBE 322A D93D 6F25 77A4 5EEB 5ED5 B41E 3C64`


## S/MIME Keys
### mail@lauka.net

Links:
- [www.lauka.net](https://www.lauka.net/certificates/smime/mail@lauka.net.crt)


Fingerprint: 
- sha256 `80824F76FFC7C0A284E3D509C72D9FBCD15FD171BC9C2AFCCCBB202BF239A659`
- sha1 `E37E4B05BA5F4ED1DDE02E1C2029E2718D676B36`


### laura@laurakalb.de

Links:
- [www.lauka.net](https://www.lauka.net/certificates/smime/laura@laurakalb.de.crt)


Fingerprint: 
- sha256 `9D998F12F668D63E4CFBA2A97B241DD16019718E35CE1A84B6EE3DB4DCCE9947`
- sha1 `080D2C468B5600EF8D19915255C96693093BDDEC`
