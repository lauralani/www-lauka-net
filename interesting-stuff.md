---
layout: layouts/home.njk
title: "Lauras Interesting Stuff"
eleventyExcludeFromCollections: true
date: Last Modified
---
[⇠ back home](/)


# Lauras Interesting Stuff
A list of cool things, curated by me 🌠

## Stuff
- [FireZone](https://www.firezone.dev/): Wireguard-GUI and config generator
- [Calcure](https://github.com/anufrievroman/calcure): TUI calendar app, installed via pip. Currently lacks support for `.ics` and external calendars.
- [LittleLink](https://demo.littlelink-custom.com/): linktree alternative with php
- [YourSpotify](https://github.com/Yooooomi/your_spotify): Spotify Dashboard for statistics and fun
- [MDwiki](https://dynalon.github.io/mdwiki/#!faq.md) oder [Gollum](https://github.com/gollum/gollum): Wikis
- [today](https://git.sr.ht/~sotirisp/today): shell app for journaling.
- [devShort](https://git.sr.ht/~rwa/devshort): url shortener that looks intriguing ^-^
- [Ursus](https://git.sr.ht/~whiisker/ursus): webapp for managing PowerDNS
- [chiaki](https://git.sr.ht/~thestr4ng3r): multi platform PS4 and PS5 remote play (feature complete)
- [undocker](https://git.sr.ht/~motiejus/undocker): docker to rootfs converter
- [Matrix: dendrite](https://matrix.org/docs/projects/server/dendrite): 2nd gen Matrix server
- [ergo](https://ergo.chat/): Modern IRC implementation in Go

## CSS
- [Design tools and Tips for Developers in a Hurry](https://github.com/sw-yx/spark-joy)
- [Halfmoon](https://www.gethalfmoon.com/docs/) Framework which Codeberg based its design on
- [CSS Patterns](https://css-pattern.com/)

## Fonts
- [Comic Mono](https://dtinth.github.io/comic-mono-font/) Comic Sans in good, as a monospace font
- [Fantasque Sans Mono](https://github.com/belluzj/fantasque-sans) wibbly-wobbly handwriting-like fuzziness that makes it unassumingly cool

## Hugo Themes
- [https://adityatelange.github.io/hugo-PaperMod/](https://adityatelange.github.io/hugo-PaperMod/)
- [https://hugo-profile.netlify.app/](https://hugo-profile.netlify.app/)
- [https://themes.gohugo.io/themes/hugo-theme-zen/](https://themes.gohugo.io/themes/hugo-theme-zen/)
- [https://themes.gohugo.io/themes/risotto/](https://themes.gohugo.io/themes/risotto/)
- [https://themes.gohugo.io/themes/hugo-theme-texify/](https://themes.gohugo.io/themes/hugo-theme-texify/)
- [https://themes.gohugo.io/themes/hugo-vitae/](https://themes.gohugo.io/themes/hugo-vitae/)
- [https://themes.gohugo.io/themes/papercss-hugo-theme/](https://themes.gohugo.io/themes/papercss-hugo-theme/)
- [https://github.com/guangzhengli/hugo-theme-ladder](https://github.com/guangzhengli/hugo-theme-ladder)
- [https://themes.gohugo.io/themes/hugo-flex/](https://themes.gohugo.io/themes/hugo-flex/): Simple Website template
- [A4 Resume](https://themes.gohugo.io/themes/resume-a4/): Resume Template

## Mail
- [aerc](https://sr.ht/~rjarry/aerc) is an email client for your terminal

## Misc
- [yet-another-bench-script](https://github.com/masonr/yet-another-bench-script) Benchmarking script that tests Storage, Network speed and CPU
- [endlessh](https://github.com/skeeto/endlessh) SSH tarpit
