---
layout: layouts/home.njk
title: "Sprachtherapie - lauka.net"
eleventyExcludeFromCollections: true
---

# Sprachtherapie
## Ziel
Ziel der Sprachtherapie bei trans Frauen ist das Ändern von Sprachmustern, um eine femininere Wahrnehmung der Stimme zu erreichen.

### Anamnesesitzung
- Vorlesen eines Textes als Referenz und zum Bestimmen von Atem- und Sprachpatterns
- Besprechen von Therapiezielen
- Setzen der Erwartungshaltung (es werden keine physischen Änderungen passieren, etc.)

### Therapiesitzung #1
Ziel: Verlagern der Aussprache von Vokalen in die vordere Mundhälfte


- Gähnen mit offenem Mund
- Kiefer dehnen durch Drücken der Handballen in die Backenregion
- "Daumensprechen/Korkensprechen": Sprechen mit Daumen oder Korken zwischen den Zähnen, verlagert kurzfristig die Aussprache nach vorne
- ein stimmloses |h| vor (mit Vokalen beginnenden) Wörtern vorstellen
- Position der Zunge sollte möglichst flach sein, um den Klangraum im Mund nicht einzuschränken

### Therapiesitzung #2
Ziel: bewusstere Betonung von (kurzen) Fragen und Aussagen


- Verringerung des Tempos durch deutliche Artikulation des Satzes (alle Laute aussprechen)
- Mini-Pausen zwischen den Wörtern
- Satzzeichen ausnutzen
- Unklare oder zu schnelle Aussprache wird allgemein als maskulin gelesen
- Variation der Stimmhöhe beim Satzende:
    - Frage: Stimme nach unten
    - Aussage: Stimme leicht nach oben
    - Bei starker Betonung des Satzes umgekehrt.

### Therapiesitzung #3
Ziel: Entspannung des Kehlkopfes -> weichere Stimme für kurze Zeit

- betontes Summen durch einen Schlauch (>= 1cm Ø) in ein Wasserbehältnis für ca. 30 Sek, z.B. "alle meine Entchen"
- große Kaubewegungen und dabei ein |m| summen, gern auch Kaugummi kauen dabei -> Silben mit |m| beginnend langziehen
