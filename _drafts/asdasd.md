---
layout: layouts/home.njk
title: "Kink and ChatGPT - lauka.net"
eleventyExcludeFromCollections: true
---

# Kink and ChatGPT

I decided to make ChatGPT generate some submissive rules and rules for safe play.

These are the results:

## Submissive Rules
Prompt: `generate 20 submissive rules for a kinky relationship`


- Always address your dominant partner as "Sir" or "Madam"
- Obedience to all commands and orders given by your dominant partner
- Always ask for permission before engaging in any sexual activity
- Complete submission to all forms of physical punishment and discipline
- Speak only when spoken to, and always in a submissive tone
- Present yourself in a respectful and obedient manner at all times
- Complete surrender of control over all decisions and actions to your dominant partner
- Attend all training sessions and BDSM play as instructed
- Do not initiate physical contact or sexual activity without permission
- Submit to all forms of physical and emotional exploration as instructed by your dominant partner
- Keep all aspects of your relationship and any BDSM play confidential
- Wear only the clothing and accessories designated by your dominant partner
- Complete honesty and transparency in all aspects of your relationship
- Willingness to explore new forms of BDSM play and sexual exploration
- Willingness to participate in public displays of submission
- Submit to regular body inspections to ensure proper grooming and hygiene
- Comply with all rules and protocols established by your dominant partner
- Willingness to engage in physical, emotional and mental submission to your dominant partner
- Be open-minded and respectful to other BDSM communities and individuals
- Always express gratitude and appreciation to your dominant partner for their guidance and control.

## Rules For Safe, Sane And Consensual Play
Prompt: `generate safe rules for kinky play`

- Communication: Open, clear and honest communication between all parties involved is key to having a safe and enjoyable kinky experience.
- Consent: All participants must give enthusiastic and clear consent for any activities that take place.
- Boundaries: Respect each other's boundaries and always have a safe word or gesture in place to indicate when to stop.
- Preparation: Do research and educate yourself on the activities you're interested in, and make sure you have the proper equipment and safety measures in place.
- Aftercare: Take care of each other emotionally and physically after engaging in kinky play, to ensure everyone feels comfortable and safe.
- Trust: Only engage in kinky play with someone you trust and with whom you have a strong relationship.
- Health: Practice safe sex and prioritize physical safety, especially if engaging in activities that involve penetration or high levels of physical intensity.
- Sobriety: Do not engage in kinky play while under the influence of drugs or alcohol, as it can impair judgement and consent.
