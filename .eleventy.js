const { DateTime } = require("luxon");

module.exports = function(eleventyConfig) {
    eleventyConfig.addPassthroughCopy({ "static/.well-known": ".well-known" })
    eleventyConfig.addPassthroughCopy({ "static/certificates": "certificates" })
    eleventyConfig.addPassthroughCopy({ "static/css": "css" })
    eleventyConfig.addPassthroughCopy({ "static/robots.txt": "robots.txt" })


    eleventyConfig.addFilter("timestamp", function(value) {
      return DateTime.fromJSDate(value).toFormat('LLLL d, yyyy HH:mm ZZZZ')
    });

    return {
      passthroughFileCopy: true
    }
  }
