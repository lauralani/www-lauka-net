# www.lauka.net

## Deploy Status
[![CI Status Badge](https://ci.codeberg.org/api/badges/lauralani/www-lauka-net/status.svg)](https://ci.codeberg.org/lauralani/www-lauka-net)

## About
This is the source code for my personal website. 

It's an entirely static webpage built with [Eleventy](https://www.11ty.dev/) and hosted with Caddy. 
