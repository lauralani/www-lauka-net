---
layout: layouts/book.njk
tags: book
author: Timothy Zahn
name: "Star Wars: Thrawn Ascendancy - Lesser Evil"
goodreads: https://www.goodreads.com/book/show/55213763-lesser-evil
rating: 5
date: 2022-05-10
---

Simply amazing.
The way Timothy Zahn orchestrates his books is unbelievably awesome, and they usually leave me pretty emotional and longing for more. This trilogy has been exactly this, and it makes me want to re-read his other books as well with all the tie-ins he makes. 

As this is the final book of another awesome trilogy by Timothy Zahn: 5/5
