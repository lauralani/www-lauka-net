---
layout: layouts/book.njk
tags: book
author: "Alice Oseman"
name: "Heartstopper (Books 1-4)"
goodreads: https://www.goodreads.com/series/236821-heartstopper
rating: 4
date: 2022-10-11
---

Heartstopper is about a young pair of boys that discover their love for eachother. The emotions and actions portrayed by the protagonists are so genuine that they almost hurt when reading the books! The book has a really nice setting, and it describes young love really nice :)
Only downside for the books is that they are in comic form, which shortens the read time by a lot. 

Solid 4/5, would be 5/5 if the books were text instead of comic form
