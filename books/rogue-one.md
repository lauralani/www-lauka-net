---
layout: layouts/book.njk
tags: book
author: Alexander Freed
name: "Star Wars: Rogue One"
goodreads: https://www.goodreads.com/book/show/30008713-rogue-one
rating: 4
date: 2021-10-16
---

While I really enjoyed Rogue One as a movie (I'd go as far as to say it's my favourite Star wars film so far), I enjoyed the book even more. It adds a lot of backstory and detail, and the characters are more fleshed out because of it. 

Overall a solid 4/5
